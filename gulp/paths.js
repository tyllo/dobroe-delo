module.exports = {

  // настройка для минификации
  uglify: {
    jade: false,
    styles: false,
    scripts: false
  },
	
  src: 'src',
  dest: 'dist',
	assets: 'dist/assets',
	images: '/images',
	scripts: '/js',
	styles: '/css',
  fonts: '/fonts'
}
