var webpack = require("webpack");
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var BowerWebpackPlugin = require("bower-webpack-plugin");

var paths = require("./paths.js");

module.exports = {
  entry: paths.src + "/index.js",
  output: {
    path: paths.assets,
    filename: "bundle.js"
  },
  resolve: {
    extensions: ['', '.js']
  },
  module: {
    loaders: [
      { test: /\.vue$/, loader: "vue" },
      { test: /\.jade/, loader: "template-html-loader" },
      { test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader' // <- without es6 polyfills
        // loader: 'babel-loader?optional=runtime' // <- contain es6 polyfills
      },
      { test: /\.(scss|sass)$/,
        loader: ExtractTextPlugin.extract({
          // activate source maps via loader query
          //'css?sourceMap!' + 'sass?sourceMap'
          loader: "style!css!sass?indentedSyntax"
        })
      },
      { test: /\.css$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader')
      },
      { test: /.*\.(gif|png|jpe?g|svg)$/i,
        loaders: [
          'file?hash=sha512&digest=hex&name=[hash].[ext]',
          'image-webpack?{progressive:true,' +
          'optimizationLevel: 7,' +
          'interlaced: false,' +
          'pngquant:{quality: "65-90", speed: 4}}'
        ]
      }
    ]
  },
  // http://habrahabr.ru/post/245991/
  plugins: [
    // extract inline css into separate 'styles.css'
    new ExtractTextPlugin('styles.css'),
    //
    //new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.DedupePlugin(),
    //new webpack.DefinePlugin({
    //  'NODE_ENV': JSON.stringify('production')
    //}),
    new BowerWebpackPlugin({
        modulesDirectories: ['bower_components'],
        manifestFiles: ['bower.json', '.bower.json'],
        includes: /.*/,
        // excludes: /.*\.sass$/
     }),
  ],
  devtool: 'source-map'
}
