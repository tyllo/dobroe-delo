var plumber = require('gulp-plumber');
var notify = require('gulp-notify');

module.exports = function () {
  return plumber({
    errorHandler: notify.onError({
      title: '<%= error.name %>',
      message: '<%= error.message %>',
      sound: true,
    })
  })
};
