var gulp = require('gulp');
var errorHandler = require('../helpers/errorHandler');
var config = require('../config').scripts;

var rigger = require('gulp-rigger');
var minify = require('gulp-uglify');

var gulpif = require('gulp-if');

gulp.task('scripts', function () {
  gulp.src(config.src)
    .pipe(errorHandler())
    //.pipe(rigger())
    .pipe(gulpif(config.minify, minify()))
    .pipe(gulp.dest(config.dest));
});
