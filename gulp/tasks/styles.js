var gulp = require('gulp');
var errorHandler = require('../helpers/errorHandler');
var config = require('../config').styles;

var sass = require('gulp-sass');
var sourcemaps  = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var minify = require('gulp-minify-css');

var gulpif = require('gulp-if');

gulp.task('styles', function () {
  gulp.src(config.src)
    .pipe(errorHandler())
    .pipe(sourcemaps.init())
    .pipe(sass(config.settings))
    .pipe(sourcemaps.write({includeContent: false}))
    .pipe(sourcemaps.init({loadMaps: true}))
    .pipe(autoprefixer(config.autoprefixer))
    .pipe(sourcemaps.write('.'))
    .pipe(gulpif(config.minify, minify()))
    .pipe(gulp.dest(config.dest));
});
