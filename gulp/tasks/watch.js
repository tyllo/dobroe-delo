var gulp = require('gulp');
var config = require('../config');

gulp.task('watch', function () {
  //gulp.watch(config.scripts.watch, ['webpack']).on('change', reload);
  gulp.watch(config.scripts.watch, ['scripts', reload]);
  gulp.watch(config.styles.watch, ['styles', reload]);
  gulp.watch(config.jade.watch, ['jade', reload]);
  gulp.watch(config.copy.watch, ['copy', reload]);
});
