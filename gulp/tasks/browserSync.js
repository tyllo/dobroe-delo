var gulp = require('gulp');
var config = require('../config');

// http://www.browsersync.io
var browserSync = require('browser-sync').create();
reload = browserSync.reload;

gulp.task('browserSync', function () {
  browserSync.init(config.browserSync);
});
