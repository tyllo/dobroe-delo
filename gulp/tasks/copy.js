var gulp = require('gulp');
var errorHandler = require('../helpers/errorHandler');
var config = require('../config').copy;

var imagemin = require('gulp-imagemin');
var cache = require('gulp-cache');

gulp.task('copy', function () {
  gulp.start('copy:scripts');
  gulp.start('copy:styles');
  gulp.start('copy:fonts');
  gulp.start('copy:images');
});

gulp.task('copy:scripts', function () {
  gulp
    .src(config.scripts.src)
    .pipe(gulp.dest(config.scripts.dest));
});
gulp.task('copy:styles', function () {
  gulp
    .src(config.styles.src)
    .pipe(gulp.dest(config.styles.dest));
});
gulp.task('copy:fonts', function () {
  gulp
    .src(config.fonts.src)
    .pipe(gulp.dest(config.fonts.dest));
});
gulp.task('copy:images', function () {
  gulp
    .src(config.images.src)
    .pipe(errorHandler())
    .pipe(//cache(
		imagemin({
         optimizationLevel: 5,
        // for .jpg
        progressive: true,
        // for .gif
        interlaced: true,
        svgoPlugins: [{removeViewBox: false}],
        // use: [pngquant()]
		})
	)//)
    .pipe(gulp.dest(config.images.dest));
});
