var gulp = require('gulp');
var config = require('../config').jade;
var errorHandler = require('../helpers/errorHandler');

var gulpJade = require('gulp-jade');
var jade = require('jade');
var sass = require('gulp-sass');

var gulpif = require('gulp-if');
var minify = require('gulp-minify-html');

// фильтр :sass для jade файлов
jade.filters.sass = function (str) {
  var result = sass.compiler.renderSync({
    data: str,
    outputStyle: 'compressed'
  });
  return result.css.toString();
};

gulp.task('jade', function () {
  var YOUR_LOCALS = {};
  gulp
    .src(config.src)
    .pipe(errorHandler())
    .pipe(gulpJade({
      locals: YOUR_LOCALS,
      jade: jade,
      pretty: true
    }))
    .pipe(gulpif(config.minify, minify(config.minifyOpt)))
    .pipe(gulp.dest(config.dest));
});
