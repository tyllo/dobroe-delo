var paths = require('../paths');

var dir = '/scss';

module.exports = {
  src:   paths.src + dir + '/**/*',
  dest:  paths.assets + paths.styles,
  watch: paths.src + dir + '/**',
  settings: {
    includePaths: [
      './bower_components/foundation/scss',
      './bower_components/owl.carousel/dist/assets',
    ],
    errLogToConsole: true,
  },
  autoprefixer: {
    browsers: ['last 5 versions']
  },
  uglify: paths.uglify.styles
}
