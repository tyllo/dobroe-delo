var paths = require('../paths');
var modRewrite = require('connect-modrewrite');

module.exports = {
  port: 3030,
  ghostMode: true,
  server: {
    open: true,
    notify: false,
    baseDir: paths.dest,
    index: "index.html",
    middleware: [
      modRewrite(['!\\.\\w+$ /index.html [L]'])
    ],
  },
};
