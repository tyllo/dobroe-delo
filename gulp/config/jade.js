var paths = require('../paths');

module.exports = {
  src:   paths.src + '/*.jade',
  dest:  paths.dest,
  watch: paths.src + '/**/*.jade',
  uglify: paths.uglify.jade,
  minifyOpt: {
    conditionals: true,
    //spare:true
  }
};
