var paths = require('../paths');

module.exports = {
  scripts: {
    src: [
      './bower_components/jquery/dist/jquery.min.js',
      './bower_components/owl.carousel/dist/owl.carousel.min.js',
    ],
    dest:  paths.assets + paths.scripts + '/vendors/',
    watch: []
  },
  styles: {
    src: [],
    dest:  paths.assets + paths.styles + '/',
    watch: []
  },
  fonts: {
    src:   paths.src + paths.fonts + '/**/*',
    dest:  paths.assets + paths.fonts + '/',
    watch: paths.src + paths.fonts + '/**/*'
  },
  images: {
    src:   paths.src + paths.images + '/**/*.*',
    dest:  paths.assets + paths.images,
    watch: paths.src + paths.images + '/**/*'
  },
};
