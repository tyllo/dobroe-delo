var paths = require('../paths');

module.exports = {
  src:    paths.src + paths.scripts + '/**',
  dest:   paths.assets + paths.scripts,
  watch:  paths.src + paths.scripts + '/**',
  uglify: paths.uglify.scripts
};