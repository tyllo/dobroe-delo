var paths = require('./paths');

var config = {};
config.styles = require('./config/styles');
config.scripts = require('./config/scripts');
config.jade = require('./config/jade');
config.copy = require('./config/copy');
config.browserSync = require('./config/browserSync');
config.webpack; // = require('./webpack');

module.exports = {
  src:     paths.src,
  dest:    paths.dest,
  assets:  paths.assets,

  copy:    config.copy,
  scripts: config.scripts,
  styles:  config.styles,
  jade:    config.jade,
  browserSync: config.browserSync,
  webpack: config.webpack
};
